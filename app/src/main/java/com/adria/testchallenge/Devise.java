package com.adria.testchallenge;

public class Devise {
    public String name;
    private double rate;
    public String base;
    public String date;


    public String getName() {
        return name;
    }

    public double getRate() {
        return rate;
    }

    public String getBase() {
        return base;
    }

    public String getDate() {
        return date;
    }

    public Devise(String name, double rate, String date) {
        this.name = name;
        this.rate = rate;
        this.date = date;
    }
}
